from django.db import models
from django.conf import settings

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=255)
    post_url = models.URLField(max_length=200, null=True)
    created_at = models.DateTimeField(auto_now_add = True)
    content = models.CharField(max_length=5000)
    def __str__(self):
        return f'{self.title} ({self.created_at})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date_comment = models.DateTimeField(auto_now_add = True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=255)
    posts = models.ManyToManyField(Post)
    def __str__(self):
        return f'"{self.name}"'
