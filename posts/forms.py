from django.forms import ModelForm
from .models import Post, Comment


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'post_url',
            'content',
        ]
        labels = {
            'title': 'Título',
            'post_url': 'Post URL da Imagem',
            'content': 'Conteúdo:',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Headbanger',
            'text': 'Comentário',
        }